These configuration files written in toml language are intended for a t-rex server

[**t-rex**](https://github.com/t-rex-tileserver/t-rex/blob/master/README.md) is, quoting its creators words, *a vector tile server specialized on publishing MVT tiles from your own data*.

I used it to generate MVT tiles from a large dataset aiming to produce a relief representation based on an irregular triangular network. Look up the [presentation](https://gitlab.com/baobabet/bits-and-pieces/-/blob/main/relief-representation-and-geometry-simplification/main-presentation.pdf) (page 10 of pdf, in french but easily understandable) for more details.
