## Content description

1. [**main-presentation.pdf**](https://gitlab.com/baobabet/bits-and-pieces/-/blob/main/relief-representation-and-geometry-simplification/main-presentation.pdf) (french): This file contains graphical schematic representations of the whole work:
    -  Relief representation using a vectorial approach (self-created methodology)
    -  Relief representation using a raster approach (Mapbox utilities)
    -  Geometry simplification for vector tile creation (using Python/Django)
    - 
2. [**paper.pdf**](https://gitlab.com/baobabet/bits-and-pieces/-/blob/main/relief-representation-and-geometry-simplification/paper.pdf) (french): Detailed explanation of the previous document

3. [**paper-annex.pdf**](https://gitlab.com/baobabet/bits-and-pieces/-/blob/main/relief-representation-and-geometry-simplification/paper-annex.pdf) (french): Contains extra information related to the document above, covering the following items:
    - Table of all software tools which has been used through the process
    - Extra drawings and illustrations
    - Pieces of produced code : gdal, Python, Python/Django, SQL requests in PostGIS, tippecanoe, t-rex and Mapbox GL-JS
    - Account of all relief data sources who have been analysed
    - Explanation of vector tiles and the system used for its indexation
    - A résumé of different simplification algorithms