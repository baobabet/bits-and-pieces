# bits and pieces

*Sample of personal work including mostly code for mapping: PostgreSQL, Python, Django, vector tiles, geographical databases and other miscellany*


* [Article](https://makina-corpus.com/blog/metier/2019/preparation-de-donnees-pour-la-creation-de-tuiles-vectorielles) about building vector tiles from raw vectorial data

* **./relief-representation-and-geometry-simplification** : more documentation and coding related to relief representation and geometry simplification
